# Xentr10

Data and scripts repository for manuscript:

- Bredeson JV, Mudd AB, Medina-Ruiz S, et al. Conserved chromatin and repetitive patterns reveal slow genome evolution in frogs. *Nat Comm*. (submitted)

Preprint:

- Bredeson JV, Mudd AB, Medina-Ruiz S, et al. Conserved chromatin and repetitive patterns reveal slow genome evolution in frogs. *bioRxiv*. 2021.10.18.464293. doi: https://doi.org/10.1101/2021.10.18.464293

To obtain all data and code, do:
```bash
git clone --recursive https://bitbucket.org/rokhsar-lab/xentr10.git
```