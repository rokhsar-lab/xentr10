##find_cent_reps.py

import pandas as pd
import gzip, getopt, sys, os, re

def main(argv):
    trf_file = ''
    cent_locs = ''
    output_dir = ''
    if len(argv) == 0:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'ht:c:o:', ['help', 'trf_file=', 'cent_locs=', 'output_dir='])
    except getopt.GetoptError as err:
        usage()
        
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        if opt in ('-t', '--trf_file'):
            trf_file = arg
            if arg.endswith('gz') == False:
                print('Please input a gzipped file.')
                sys.exit(2)
        if opt in ('-c, --cent_locs'):
            cent_locs = pd.read_csv(arg, sep='\t')
        if opt in ('-o', '--output_dir'):
            output_dir = str(arg)
            #if (os.path.isdir(output_dir) == False):
            #    os.mkdir(output_dir)
            #    print('Created directory: ', output_dir)
        if output_dir == '':
            output_dir = ''.join((trf_file[:-7],'_CONSENSUS.fa'))
    return trf_file, cent_locs, output_dir

def usage():
    print('Usage:\n\n')
    print('find_cent_reps.py -t <trf_file> -c <cent_locs> -o <output_dir>')
    print('-t\t--trf_file=<tab>\tComplete path to output of trf.')
    print('-c\t--cent_locs=<tab>\tComplete path to tab separated file containing the start and end points of each chromosoms\'s centromere(s).') 
    print("-o\t--out_dir=<str>\tComplete path to Output directory")
    print('This program finds the repeats returned in the output of TRF that are located within the centromeres.')
    sys.exit(2)
    return()

def check_cent(cent_locs, chromosome, start, end):
    toReturn = False
    starts = cent_locs[cent_locs.Chr == chromosome]['Start'].values
    ends = cent_locs[cent_locs.Chr == chromosome]['End'].values
    for i in range(len(starts)):
        if (int(starts[i]) < int(start)) &  (int(ends[i]) > int(end)):
            toReturn = True
            break
        else: 
            continue
    return toReturn
    
def find_cent_reps(trf_file, cent_locs, output_dir):
    out_file = open(output_dir, 'w')
    with gzip.open(trf_file, 'rb') as f:
        Chr = ''
        for line in f:
            line = line.decode('utf-8')
            if line == '\n':
                continue
            if 'Chr' in line:
                Chr = line.split('@')[1].replace('\n','')
                continue
            if Chr not in cent_locs.Chr.unique():
                continue
            else:
                line_ = line.split(" ")
                start, end, monomer_size, copy_number, cons_size = line_[0], line_[1], line_[2], line_[3], line_[4]
                cons_seq = line_[13]
                in_cent = check_cent(cent_locs, Chr, start, end)
                if int(monomer_size) > 50 and float(copy_number) > 2 and in_cent:
                    out_file.write(''.join(('>', Chr, ':', str(start), '-', str(end), ':', str(monomer_size), ',', str(copy_number), ',', str(cons_size), '\n')))
                    out_file.write(''.join((cons_seq, '\n')))
    out_file.close()
    
if __name__ == '__main__':
    trf_file, cent_locs, output_dir = main(sys.argv[1:])
    #output_dir = ''.join((output_dir))
    #if (os.path.isdir(output_dir) == False):
    #    os.mkdir(output_dir)
    #    print('Created OUTPUT DIRECTORY: ', output_dir,'\n')
    
    print('Input files:')
    print('trf_file:', trf_file)
    print('output_dir:', output_dir)
    find_cent_reps(trf_file, cent_locs, output_dir)
    
    print('Done!\n\n')
    
