
## centromere_TRF_Enrichment.py

import getopt, sys, os, re
import copy
import numpy as np
import pandas as pd
import scipy.stats as stats
import statsmodels.stats.multitest as smm

def main(argv):
    blast = ''
    cent_locs = ''
    output_dir = ''
    chr_length = 0
    
    if len(argv) == 0:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hb:c:i:o:', ['help', 'blast_file=', 'cent_locs=', 'genome_index=', 'output_dir='])
    except getopt.GetoptError as err:
        usage()

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit(2)
        if opt in ('-b','--blast_file'):
            blast_file = arg
            if (blast_file.endswith('blast') == False):
                print ('Please input a .blast file')
                sys.exit(2)
            if blast_file == '':
                usage()
        if opt in ('-c', '--cent_locs'):
            cent_locs = str(arg)
        if opt in ('-i', '--genome_index'):
            chr_length = str(arg)
        if opt in ('-o', '--output_dir'):
            output_dir = str(arg)
    if output_dir == '':
        print ('Please write the name of the output file')
        usage()
    return(blast_file, cent_locs, chr_length, output_dir)


def usage():
    print('usage: \n\n')
    print('This code was written by Joseph Park, Sofia Medina-Ruiz, Rokhsar Lab on July 15, 2019.\n\n')
    print('centromere_TRF_Enrichment.py -b <blast> -c <cent_locs> -o <output_dir>')
    print("-b\t--blast_file=<str>\tComplete path to BLAST file (*.fa.blast)")
    print("-c\t--cent_locs=<bed>\tTab separated file with Chromosomes and their respective centromere locations.")
    print("-i\t--genome_index=<bed>\tTGenome index file that contains chromosomes sizes (samtools index).")
    print("-o\t--out_dir=<str>\tComplete path to Output directory")
    print('This program finds the most enriched repeats within a genome\'s centromeres. It outputs a table sorted in order of most significant consensus repeats.')
    sys.exit(2)
    return()

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

def find_consensus(blast_file, chr_index_file, cent_loc_file):
    print "Reading tables"
    wiggle = 1
    Mb = float(1000000)
    Chr_cents = pd.read_csv(cent_loc_file, sep='\t', index_col=0)
    Chr_cents['Start'] = Chr_cents.Start - wiggle*Mb/2 #500Kb
    Chr_cents.loc[Chr_cents.Start<0,'Start'] = 1
    Chr_cents['End'] = Chr_cents.End + wiggle*Mb/2 #500kb

    chr_idx   = pd.read_csv(chr_index_file, sep='\t', names=(['Chr','Length','A','B','C']))
    chr_idx   = chr_idx[chr_idx.Chr.isin(Chr_cents.Chr.unique())]

    names = ['qseqid', 'Chr', 'Match percent', 'Length', 'Mismatch', 'Gapopen', 'Consensus start', 'Consensus end', 'sstart', 'send', 'Evalue', 'Bitscore']
    Blast_out = pd.read_csv(blast_file, sep='\t', names=names)
    Blast_out['midpoint'] = (Blast_out.sstart + Blast_out.send)/2
    print 'Done configurating blast output'

    Mb = float(1000000)
    total_chr_len  = float(chr_idx.Length.sum())
    total_cent_len = float(sum(Chr_cents['End'] - Chr_cents['Start']))
    total_arm_len  = float(total_chr_len -  total_cent_len)
    print "total_chr_len", total_chr_len
    print  "total_cent_len", total_cent_len
    print "total_arm_len", total_arm_len

    
    for Chr_ in Chr_cents.Chr.unique():
        Blast_out.loc[Blast_out.Chr==Chr_,'Bool'] = False
        for list_cents  in zip(Chr_cents[Chr_cents.Chr==Chr_].Start,Chr_cents[Chr_cents.Chr==Chr_].End):
            print "Identifying repeats insice centromere:", Chr_, list_cents
            Blast_out.loc[Blast_out.Chr==Chr_,'Bool'] = pd.Series(Blast_out[Blast_out.Chr==Chr_].midpoint).between(list_cents[0],list_cents[1])  |   pd.Series(Blast_out.loc[Blast_out.Chr==Chr_,'Bool'])
                                        
    #consensus = copy.copy(Blast_out)
    consensus  = Blast_out.groupby(['qseqid'])['Bool'].value_counts().unstack().fillna(0)
    consensus  = consensus.rename(columns={True:'IN_Cent',False:'NOT_Cent'})
#    print consensus.loc[['Chr8_10:50760900-50761227:57,5.8,57']]
    #in_centromere_df  = consensus[consensus['Bool'] == True]
    #out_centromere_df = consensus[consensus['Bool'] == False]
    #print(len(in_centromere_df))
    #print(len(out_centromere_df))

#    Counts_centromere    = pd.DataFrame(in_centromere_df['qseqid'].value_counts())
#    Counts_centromere    = Counts_centromere.rename(columns={'qseqid':'IN_Cent'})

#    Counts_NOTcentromere = pd.DataFrame(out_centromere_df['qseqid'].value_counts())
#    Counts_NOTcentromere = Counts_NOTcentromere.rename(columns={'qseqid':'NOT_Cent'})
    
    #Merge counts per tandem repeats
    print "Merging counts"
    #Tandem_rep_count     = pd.merge(Counts_centromere, Counts_NOTcentromere, left_index=True, right_index=True, how='outer')
    Tandem_rep_count     = copy.copy(consensus)

    Tandem_rep_count     = Tandem_rep_count.fillna(0)
    Tandem_rep_count['Not_rep_cent']     = (total_cent_len- Tandem_rep_count['IN_Cent'])
    Tandem_rep_count['Not_rep_Not_cent'] = (total_arm_len - Tandem_rep_count['NOT_Cent'])
    Tandem_rep_count['Cen_len']          = total_cent_len
    Tandem_rep_count['Arm_len']          = total_arm_len
                                                                                                       
    #Obtain ratios
    print "Obtaining ratios" 
    Tandem_rep_count['IN_Cent_p']        = Tandem_rep_count['IN_Cent'].apply( lambda x: float(x *total_cent_len) / float(total_chr_len))
    Tandem_rep_count['NOT_Cent_p']       = Tandem_rep_count['NOT_Cent'].apply(lambda x: float(x *total_arm_len)  / float(total_chr_len))
    Tandem_rep_count['Delta']  = (Tandem_rep_count.IN_Cent_p/total_cent_len)/(Tandem_rep_count.NOT_Cent_p/total_arm_len)
    
    Tandem_rep_count['Not_rep_cent_p']     = (Tandem_rep_count['Not_rep_cent'])/total_cent_len
    Tandem_rep_count['Not_rep_Not_cent_p'] = (Tandem_rep_count['Not_rep_Not_cent'])/total_arm_len
    
    
    #Fisher's exact on ratios
    print "Performing Fisher's exact test"
    #total_chr_len, total_cent_len
    Tandem_rep_count['Pvalue']      = Tandem_rep_count.apply(lambda row: stats.fisher_exact([[row.IN_Cent,total_cent_len - row.IN_Cent],[row.NOT_Cent, total_arm_len - row.NOT_Cent]])[1],axis=1)
                                                             
#    Tandem_rep_count['Pvalue']      = Tandem_rep_count.apply(lambda row: stats.fisher_exact([[row.IN_Cent_p,row.NOT_Cent_p],[total_cent_len,total_arm_len]])[1], axis=1)
    
    #    Tandem_rep_count['Pvalue']      = Tandem_rep_count.apply(lambda row: stats.fisher_exact([[row.IN_Cent_p, row.NOT_Cent_p], [row.Not_rep_cent_p,row.Not_rep_Not_cent_p]])[1], axis=1)
    Tandem_rep_count['Bonf_Pvalue'] = Tandem_rep_count.Pvalue*Tandem_rep_count.shape[0]
    Tandem_rep_count['fdr_bh']      = smm.multipletests(Tandem_rep_count.Pvalue.to_list(), alpha=0.05, method='fdr_bh')[1]
    return (Tandem_rep_count)



if __name__ == '__main__':
    blast_file, cent_locs, chr_length,  output_dir = main(sys.argv[1:])
    
    print ("Input files:")
    print ('Blast file:\t', blast_file)
    print ('cent_locs:\t',cent_locs)
    print ('chr_length:\t', chr_length)
    print "... Starting process of finding centromeric enrichment\n"
    consensus_reps_df = find_consensus(blast_file, chr_length, cent_locs)
    print "\n\nSaving results as:",output_dir
    consensus_reps_df.to_csv(output_dir, sep='\t')

    print ('Done finding enriched repeats!\n\n')
