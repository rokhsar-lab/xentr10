import sys

in_fasta = sys.argv[1]
out_file_name = ''.join((''.join((in_fasta.split('.')[:-1])),'_X2.fa'))


file = open(out_file_name, 'w')


with open(in_fasta, 'r') as f:
    for line in f:
        line = line.strip('\n')
        if ">" in line:
            file.write(''.join((line, "_X2", '\n')))
        else:
            file.write(''.join((line, line, '\n')))
file.close()

print "Created fasta with 2Xseq as:\t", out_file_name
print "To run:\n"

out_Blast_clust = ''.join((out_file_name,'_BC_nr.list'))

print '~/SCRIPTS/Programs/blast-2.2.26/bin/blastclust -a 8 -i ',out_file_name,' -S 75  -p F -L .45 -b F -W 10 -o ', out_Blast_clust, "&& cat ",out_Blast_clust," | sed s/\'_X2\'//g >CONSENSUS_X2.fa_BC_nr.out"
