##refine_len.py

import gzip, getopt, os, sys, pysam
import numpy as np
import pandas as pd

def main(argv):
    blast_clust_file = ''
    consensus_seq_file = ''
    output_dir = './'
    if len(argv) == 0:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hb:c:o:', ['help', 'blast_clust_file=', 'consensus_seq_file=', 'output_dir='])
    except getopt.GetoptError as err:
        usage()
        
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        if opt in ('-b', '--blast_clust_file'):
            blast_clust_file = arg
        if opt in ('-c', '--consensus_seq_file'):
            consensus_seq_file = arg
        if opt in ('-o', '--out'):
            output_dir = str(arg)
            if (os.path.isdir(output_dir) == False):
                os.mkdir(output_dir)
                print('Created directory: ', output_dir)
            
    return blast_clust_file, consensus_seq_file, output_dir

def usage():
    print('usage:\n\n')
    print('refine_len.py -b <blast_clust_file> -c <consensus_seq_file> -o <output_dir>')
    print('-b\t--blast_clust_file=<tab>\tComplete path to output file of BLASTclust.')
    print('-c\t--consensus_seq_file=<tab>\tComplete path to file containing consensus coordinates and repeat sequences.')
    print('-o\t--output_dir=<str>Complete path to output directory.')
    print('This porgram takes the clusters given by BLASTclust and returns a representative(first instance) sequence of the length which is most commonly repeated in each cluster.')
    sys.exit(2)
    return()

def refine_lengths(blast_clust_file, consensus_seq_file, output_dir):

    CONSENSUSblastClust = []
    with open(blast_clust_file, 'r') as f:
        for line in f:
            #line = line.decode('utf-8')
            #lst = line.replace(' \n', "").split(' ')
            lst = line.strip('\n').split(' ')
            CONSENSUSblastClust.append(lst[:-1])

            
    CONSENSUSblastClust_dict = {}
    for i in range(len(CONSENSUSblastClust)):
        lines = []
        for line in CONSENSUSblastClust[i]:
#            if line == ' ':
#                continue
            line_split = line.split(':')
            monomer_size = int(str(line_split[2]).split(',')[0])
            line_ = line_split.append(monomer_size)
            #Chr:Start-End:monomer_size, repnum, consSize
            lines.append(line_)
        print lines
        df1 = pd.DataFrame(lines, columns=['Chr','Start-End', 'MS_Repeat_no_Consensus_size', 'Monomer_size'])
        CONSENSUSblastClust_dict[''.join(('Cluster ', str(i)))] = int(df1['Monomer_size'].value_counts().index[0])
        
    toReturn = []
    for i in range(len(CONSENSUSblastClust_dict)):
        for line in CONSENSUSblastClust[i]:
            length = line.split(',')[1]
            if int(length) == CONSENSUSblastClust_dict[''.join((('Cluster '), str(i)))]:
                toReturn.append(line)
                break
                
    consensus_seq_file_name = consensus_seq_file
    c = pysam.FastaFile(consensus_seq_file_name)
    
    sig_reps_file = open(''.join((output_dir, '/', output_dir, '_sig_len_reps.fa')), 'w')
    for rep in toReturn:
        try:
            sig_reps_file.write(''.join(('>', rep, '\n')))
            sig_reps_file.write(''.join((c.fetch(rep), '\n')))
        except KeyError:
            continue
    sig_reps_file.close()
                                 
if __name__ == '__main__':
    blast_clust_file, consensus_seq_file, output_dir = main(sys.argv[1:])
    output_dir = ''.join((output_dir))
    if (os.path.isdir(output_dir) == False):
        os.mkdir(output_dir)
        print('Created OUTPUT DIRECTORY: ', output_dir,'\n')
    
    print('Input files:')
    print('blast_clust_file:\t', blast_clust_file)
    print('consensus_seq_file:\t', consensus_seq_file)
    print("output_dir:/t", output_dir)
    refine_lengths(blast_clust_file, consensus_seq_file, output_dir)
    
    print('Done!!\n\n')

    
            
