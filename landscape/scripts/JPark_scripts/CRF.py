import argparse, os
import time
import sys

PATH_SCRIPTS = '/global/homes/s/sofiamr/SCRIPTS/Python_scripts/JPark_scripts/'
MODULES = 'module load python/2.7-anaconda-4.4 && source activate Cori_new '

# May 18, 2020 - Sofia Medina 
# Rokhsar lab - UC Berkeley
# To run: module load python/2.7-anaconda-4.4 && source activate Cori_new && python CRF.py -s XENTRv10-1 -p /global/cscratch1/sd/sofiamr/SAPS/X_tropicalis_V10_Final/IGC_7/TRF_Out/
# module load python/2.7-anaconda-4.4 && source activate Cori_new && python CRF.py -s XENTRv10-1 -p /global/cscratch1/sd/sofiamr/SAPS/X_tropicalis_V10_Final/IGC_7/TRF_Out/ -c XENTRv10-1_Centurion4_15Mb.tab

def parse_args():
    parser = argparse.ArgumentParser(description='Centromeric Repeat Finder')
    parser.add_argument("-f", "--genome_fa", metavar='STR', help='Fasta of chromosomes. Eg. path_wd/Specie_ID_Chrs_only.fa', type=str, default='')
    parser.add_argument("-i", "--genome_index", metavar='STR', help='Chromosomes index file. Eg. path_wd/Specie_ID_Chrs_only.fa.fai', type=str, default='')
    parser.add_argument("-c", "--centromere_bound", metavar='STR', help='Table of chromosome regions deliminting pericentromeric locations', type=str, default='')
    parser.add_argument("-t", "--trf_loc", metavar='STR', help='TRF output file', type=str, default='')
    parser.add_argument("-e", "--executable_loc", metavar='STR', help='path to CRF scripts', type=str, default='')
    parser.add_argument("-v", "--version", action='version', version='%(prog)s 1.0')
    parser.add_argument("-s", "--Specie_ID", metavar='STR', help='Prefix that serves as identifier for files', type=str, default='XENTRv10-1')
    parser.add_argument("-p", "--path_wd", metavar='STR', help='Working directory path', type=str, default='/global/cscratch1/sd/sofiamr/SAPS/X_tropicalis_V10_Final/IGC_7/TRF_Out/')
    parser.add_argument("-m", "--module_load", metavar='STR', help='Working directory path', type=str, default='module load python/2.7-anaconda-4.4 && source activate Cori_new')
    args = parser.parse_args() 
    return(args)


def main():
    args = parse_args()
    Specie_ID = args.Specie_ID
    path_wd = ''.join((args.path_wd,'/')).replace('//','/')
    genome_db_chrs_only = args.genome_fa
    genome_index_loc = args.genome_index
    Centromeric_locations = args.centromere_bound
    
    if args.genome_fa == '':
        genome_db_chrs_only = ''.join((path_wd,Specie_ID,'_Chrs_only.fa'))
    if args.genome_index == '':
        genome_index_loc = ''.join((genome_db_chrs_only,'.fai'))
    if args.centromere_bound == '':
        Centromeric_locations = ''.join((Specie_ID,'_HiC_centromeres.tab'))
    if args.trf_loc == '':
        trf_loc = ''.join((path_wd,Specie_ID,'.trf.gz'))
    if args.executable_loc == '':
        path_scripts = '/global/homes/s/sofiamr/SCRIPTS/Python_scripts/JPark_scripts/'
    else:
        path_scripts = PATH_SCRIPTS
    if args.module_load == '':
        module_load = 'module load python/2.7-anaconda-4.4 && source activate Cori_new '
    else:
        module_load = MODULES
    
    
    list_files = [genome_db_chrs_only, genome_index_loc,Centromeric_locations, trf_loc,  path_scripts]
    for f in list_files:
        if os.path.exists(f) == False:
            print "File not found:", f
            print "Please check your files"
            sys.exit(2) 
    
    print make_all_commands_for_TRF_enrichment_in_Centromere_v2(Specie_ID, path_wd, genome_db_chrs_only, genome_index_loc, Centromeric_locations, trf_loc, path_scripts, module_load)
    return()

def make_all_commands_for_TRF_enrichment_in_Centromere_v2(Specie_ID, path_wd, genome_db_chrs_only, genome_index_loc, Centromeric_locations, trf_loc, path_scripts, module_load):
    
    num_threads = 8
    
    ## Paths for executables ### Dependent on environment
    blastclust_loc        = '~/SCRIPTS/Programs/blast-2.2.26/bin/blastclust'
    find_cent_reps_path   = ''.join((path_scripts,'find_cent_reps.py'))
    refine_len_path       = ''.join((path_scripts,'refine_len.py'))
    centromereEnrich_path = ''.join((path_scripts,'centromere_TRF_Enrichment_smr.py'))
    
    fasta_cmd1_out    = ''.join((path_wd,Specie_ID[:3],'_CONSENSUS.fa'))
    fasta_cmd1_out_2x = ''.join((''.join((fasta_cmd1_out.split('.')[:-1])),'_X2.fa'))

    
    output_cmd2       = ''.join((fasta_cmd1_out[:-3], '_Clust.out')) 
    fasta_cmd3_out    = ''.join((output_cmd2[:-4],'_represent.fa',))
    output_blast_cmd4 = ''.join((fasta_cmd3_out[:-3],'.blast'))
    final_output_name = output_blast_cmd4.replace('.blast','_TRF_Enriched_Cent.tab')
    
    if os.path.exists(''.join((genome_db_chrs_only,'.nhr'))) == False:  #check if fasta database exists:
        module_load_ = ''.join((module_load, ' && '))
        make_blast_db = ''.join(( module_load_, ' makeblastdb  -dbtype nucl -in ', genome_db_chrs_only ,' && '))
    else:
        make_blast_db = ''
    
    
    cmd_0 = ''.join(('cd ',path_wd))
    cmd_1 = ''.join(('python ', find_cent_reps_path,' -t ', trf_loc, ' -c ', Centromeric_locations, ' -o ', fasta_cmd1_out))    
    cmd_2x_preBC = ''.join(('python ~/SCRIPTS/Python_scripts/JPark_scripts/Make_non_redundant_consensus.py ', fasta_cmd1_out))  ###Out: fasta_cmd1_out_2x
    cmd_2 = ''.join((blastclust_loc, ' -a ', str(num_threads) ,' -i ', fasta_cmd1_out_2x, ' -S 75  -p F -L .45 -b F -W 10 -o ',  fasta_cmd1_out_2x.replace('.fa','_Clust.out'), ' && cat ', fasta_cmd1_out_2x.replace('.fa','_Clust.out'), " | sed s/\'_X2\'//g >" , output_cmd2 ))
    cmd_3 = ''.join(('python ', refine_len_path, ' -b ', output_cmd2, ' -c ', fasta_cmd1_out, ' -o ', fasta_cmd3_out))
    cmd_4 = ''.join((make_blast_db, 'blastn -query ', fasta_cmd3_out, ' -db ', genome_db_chrs_only , ' -outfmt 6 -evalue 1e3 -num_threads ' ,str(num_threads),' > ', output_blast_cmd4))
    cmd_5 = ''.join(('python ', centromereEnrich_path, ' -b ', output_blast_cmd4, ' -c ', Centromeric_locations, ' -i ', genome_index_loc, ' -o ' , final_output_name))
    final_cmd =  ' && '.join((cmd_0,module_load, cmd_1,cmd_2x_preBC, cmd_2, cmd_3, cmd_4, cmd_5))
    return(final_cmd)

if __name__=='__main__':
    main()
