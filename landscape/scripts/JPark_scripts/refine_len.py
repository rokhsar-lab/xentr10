##This program was created by Joseph Park, Sofia Medina-Ruiz, Rokhsar Lab on July 15, 2019    
##This program takes the output of the BLASTclust and the output of refine_len_sm.py and outputs a FASTA file that consists of one representative sequence for each cluster as defined by BLASTclust. The representative sequence will have the length that is seen most frequenty in the cluster, and the sequence that is outputted will be the first occurrance of a sequence of desired length in each cluster. 
# Modified July 22, 2019

import gzip, os, sys, pysam, getopt
import numpy as np
import pandas as pd

def main(argv):
    blast_clust_file = ''
    consensus_seq_file = ''
    output_dir = ''
    if len(argv) == 0:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hb:c:o:', ['help', 'blast_clust_file=', 'consensus_seq_file=', 'output_dir='])
    except getopt.GetoptError as err:
        usage()
        
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        if opt in ('-b', '--blast_clust_file'):
            blast_clust_file = arg
        if opt in ('-c', '--consensus_seq_file'):
            consensus_seq_file = arg
        if opt in ('-o', '--out'):
            output_dir = str(arg)
        if output_dir == '':
            output_dir = ''.join((consensus_seq_file.split('.')[0],'_CONSENSUS.fa'))
                                 
    return (blast_clust_file, consensus_seq_file, output_dir)

def usage():
    print('usage:\n\n')
    print('refine_len_sm.py -b <blast_clust_file> -c <consensus_seq_file> -o <output_dir>')
    print('-b\t--blast_clust_file=<tab>\tComplete path to output file of BLASTclust.')
    print('-c\t--consensus_seq_file=<tab>\tComplete path to file containing consensus coordinates and repeat sequences.')
    print('-o\t--output_dir=<str>Complete path to output directory.')
    print('This porgram takes the clusters given by BLASTclust and returns a representative(first instance) sequence of the length which is most commonly repeated in each cluster.')
    sys.exit(2)
    return()


def find_common_monomer(list_monomers):
    monomer_sizes_list = []
    for j in list_monomers:
        monomer_sizes_list.append(j.split(':')[2].split(',')[0])
        most_common = pd.Series(monomer_sizes_list).value_counts().sort_values(ascending =False).index.to_list()
    patter_to_check = ''.join((':',str(most_common[0]),','))
    
    common_consensus = pd.Series(list_monomers).loc[pd.Series(list_monomers).str.contains(patter_to_check)].to_list()
    
    rep_max = 0
    for j in common_consensus:
        if patter_to_check in j:
            if j.split(':')[2].split(',')[1]>rep_max:
                rep_max = j.split(':')[2].split(',')[1]
    
    patter_to_check_final = ''.join((':',str(most_common[0]),',',str(rep_max),','))
    most_common_consensus = pd.Series(list_monomers).loc[pd.Series(list_monomers).str.contains(patter_to_check_final)].to_list()[0]
    return(most_common_consensus)



def refine_lengths(blast_clust_file, consensus_seq_file, output_dir):

    CONSENSUSblastClust = []
    with open(blast_clust_file, 'r') as f:
        for line in f:
            lst = line.replace(' \n', "").split(' ')
            CONSENSUSblastClust.append(lst)
    
    representative_monomer = []
    for list_monomers in CONSENSUSblastClust:
        representative_monomer.append(find_common_monomer(list_monomers))
    
    print 'representative_monomers:', len(representative_monomer)
    
    sig_reps_file = open(output_dir,'w') 
    print_seq =False
    
    with open(consensus_seq_file,'r') as g:
        for rep in g:
            rep = rep.strip('\n').replace(' ','')
            if str(rep[1:]) in representative_monomer:
                header_ = ''.join((rep,'\n'))
                sig_reps_file.write(header_)
                print_seq = True
                continue
            if print_seq ==True:
                seq_ = ''.join((rep,'\n'))
                sig_reps_file.write(''.join((seq_,seq_)))
                print_seq = False
            
    sig_reps_file.close()
                                 
if __name__ == '__main__':
    blast_clust_file, consensus_seq_file, output_dir = main(sys.argv[1:])
    print('Input files:')
    print('blast_clust_file:\t', blast_clust_file)
    print('consensus_seq_file:\t', consensus_seq_file)
    print("output_dir:\t", output_dir)
    refine_lengths(blast_clust_file, consensus_seq_file, output_dir)
    
    print('Done\n\n')
