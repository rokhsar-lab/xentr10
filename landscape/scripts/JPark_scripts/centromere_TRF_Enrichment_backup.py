## centromere_TRF_Enrichment.py

import getopt, sys, os, re
import numpy as np
import pandas as pd
import scipy.stats as stats
import statsmodels.stats.multitest as smm

def main(argv):
    blast = ''
    cent_locs = ''
    output_dir = ''
    chr_length = 0
    
    if len(argv) == 0:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hb:c:i:o:', ['help', 'blast_file=', 'cent_locs=', 'genome_index=', 'output_dir='])
    except getopt.GetoptError as err:
        usage()

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit(2)
        if opt in ('-b','--blast_file'):
            blast_file = arg
            if (blast_file.endswith('blast') == False):
                print ('Please input a .blast file')
                sys.exit(2)
            if blast_file == '':
                usage()
        if opt in ('-c', '--cent_locs'):
            cent_locs = pd.read_csv(arg, sep='\t')
        if opt in ('-i', '--genome_index'):
            chr_sizes_df = pd.read_csv(arg, sep='\t', names=['Chr','Length','A','B','C'])
            chr_sizes_df = chr_sizes_df[chr_sizes_df.Chr.str.startswith('Chr')][['Chr','Length']]
            chr_length = chr_sizes_df[pd.Series([x in list(cent_locs['Chr'].values) for x in chr_sizes_df['Chr']])].Length.sum()
        if opt in ('-o', '--output_dir'):
            output_dir = str(arg)
    if output_dir == '':
        print ('Please write the name of the output file')
        usage()
    return(blast_file, cent_locs, chr_length, output_dir)


def usage():
    print('usage: \n\n')
    print('This code was written by Joseph Park, Sofia Medina-Ruiz, Rokhsar Lab on July 15, 2019.\n\n')
    print('centromere_TRF_Enrichment.py -b <blast> -c <cent_locs> -o <output_dir>')
    print("-b\t--blast_file=<str>\tComplete path to BLAST file (*.fa.blast)")
    print("-c\t--cent_locs=<bed>\tTab separated file with Chromosomes and their respective centromere locations.")
    print("-i\t--genome_index=<bed>\tTGenome index file that contains chromosomes sizes (samtools index).")
    print("-o\t--out_dir=<str>\tComplete path to Output directory")
    print('This program finds the most enriched repeats within a genome\'s centromeres. It outputs a table sorted in order of most significant consensus repeats.')
    sys.exit(2)
    return()
def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

def check_cent(Chr, start, cent):
    cent_start = cent_locs[cent_locs['Chr'] == Chr].Start.values
    cent_end = cent_locs[cent_locs['Chr'] == Chr].End.values
    for i_ in range(len(cent_start)):
        cent = (start > cent_start[i_]) and (start < cent_end[i_])
        if cent == True:
            break
    return cent

def find_consensus(blast_file, chr_length, cent_locs):
    names = ['qseqid', 'Chr', 'Match percent', 'Length', 'Mismatch', 'Gapopen', 'Consensus start', 'Consensus end', 'sstart', 'send', 'Evalue', 'Bitscore']
    total_chr_len = chr_length
    total_cent_len = sum(cent_locs['End'] - cent_locs['Start'])
    consensus = pd.read_csv(blast_file, sep='\t', names=names)
    consensus = consensus[consensus.Chr.str.startswith('Chr')]
    consensus['Strand'] = consensus.apply(lambda row: '+' if row.send > row.sstart else '-', axis=1)
    consensus_df = consensus.groupby('Chr')['Chr'].value_counts().unstack().fillna(0)
    chr_keys = natural_sort(consensus.Chr.unique())
    cent_regions_dict = dict(zip(chr_keys, cent_locs))
    consensus['Bool'] = consensus.apply(lambda row: check_cent(row.Chr, row.sstart, False), axis=1)

    in_centromere_df = consensus[consensus['Bool'] == True]
    out_centromere_df = consensus[consensus['Bool'] == False]
    print(len(in_centromere_df))
    print(len(out_centromere_df))
    Counts_centromere    = pd.DataFrame(in_centromere_df['qseqid'].value_counts())
    Counts_centromere    = Counts_centromere.rename(columns={'qseqid':'IN_Cent'})
    in_centromere        = Counts_centromere
    Counts_centromere    = Counts_centromere['IN_Cent'].apply(lambda x: x*1000000 / float(total_cent_len))
    Counts_NOTcentromere = pd.DataFrame(out_centromere_df['qseqid'].value_counts())
    Counts_NOTcentromere = Counts_NOTcentromere.rename(columns={'qseqid':'NOT_Cent'})
    out_centromere       = Counts_NOTcentromere
    Counts_NOTcentromere = Counts_NOTcentromere['NOT_Cent'].apply(lambda x: x*1000000 / float(total_chr_len))
    Tandem_rep_count     = pd.merge(Counts_centromere, Counts_NOTcentromere, left_index=True, right_index=True, how='outer')
    Tandem_rep_count     = Tandem_rep_count.fillna(0)
    Tandem_rep_count['Ratio'] = ((in_centromere['IN_Cent'] - out_centromere['NOT_Cent']) / float(in_centromere['IN_Cent'] + out_centromere['NOT_Cent']))
    Not_rep_cent = Tandem_rep_count.IN_Cent.sum()
    Not_rep_Not_cent = Tandem_rep_count.NOT_Cent.sum()
    Tandem_rep_count['Not_rep_cent'] = Tandem_rep_count['IN_Cent'].apply(lambda x: Not_rep_cent - x)
    Tandem_rep_count['Not_rep_Not_cent'] = Tandem_rep_count['NOT_Cent'].apply(lambda x: Not_rep_cent - x)
    Tandem_rep_count['Pvalue'] = Tandem_rep_count.apply(lambda row: stats.fisher_exact([[row.IN_Cent, row.NOT_Cent], [row.Not_rep_cent,row.Not_rep_Not_cent]])[1], axis=1)
    Tandem_rep_count['Bonf_Pvalue'] = Tandem_rep_count.Pvalue*Tandem_rep_count.shape[0]
    Tandem_rep_count['fdr_bh'] = smm.multipletests(Tandem_rep_count.Pvalue.to_list(), alpha=0.05, method='fdr_bh')[1]
    print(len(Tandem_rep_count))
    TanReps_Enriched_in_Cent = Tandem_rep_count[(Tandem_rep_count.IN_Cent > Tandem_rep_count.NOT_Cent) & (Tandem_rep_count.Pvalue<0.05)].index.to_list()
    print(len(TanReps_Enriched_in_Cent))
    Chr_Enriched_reps = list(pd.Series(TanReps_Enriched_in_Cent)[pd.Series(TanReps_Enriched_in_Cent).str.startswith('Chr')])
    print(len(Chr_Enriched_reps))
    return Tandem_rep_count.loc[Chr_Enriched_reps].sort_values('fdr_bh', ascending=True)

if __name__ == '__main__':
    blast_file, cent_locs, chr_length,  output_dir = main(sys.argv[1:])
    
    #consensus_out_file = ''.join((output_dir, blast_file.split('.')[0],'_CONS.tab'))
    print ("Input files:")
    print ('Blast file:\t', blast_file)
    print ('output_dir:\t', output_dir)
    print ('chr_length:\t', chr_length)
    consensus_reps_df = find_consensus(blast_file, chr_length, cent_locs)
    consensus_reps_df.to_csv(output_dir, sep='\t')

    print ('Done finding enriched repeats!\n\n')
