##This program will take the multiple alignment output of MAFFT and return the consensus sequence. 

import numpy as np
import pandas as pd
import os, pysam, getopt, sys

def main(argv):
    mafft_fa_file = ''
    bad_format = False
    output_dir = ''
    if len(argv) == 0:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hm:f:o:', ['help', 'mafft_fa_file=', 'bad_format=', 'output_dir='])
    except getopt.GetoptError as err:
        usage()
    
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit()
        if opt in ('-m', '--mafft_fa_file'):
            mafft_fa_file = arg
        if opt in ('-f', '--bad_format'):
            if str(arg) == 'T':
                bad_format = True
            elif str(arg) == 'F':
                bad_format = False
            elif arg == 'True' or arg == 'False':
                bad_format = arg
            else:
                print('Please input a boolean(True, False, T, F)!')
                sys.exit()
        if opt in ('-o', '--output_dir'):
            output_dir = str(arg)
            
    return mafft_fa_file, bad_format, output_dir
    
def usage():
    print('usage:\n')
    print('create_consensus.py -m <mafft_fa_file> -f <bad_format> -o <output_dir>')
    print('-m\t--mafft_fa_file=<fa>\tComplete path to outputted FASTA file of MAFFT.')
    print('-f\t--bad_format=<Bool>\tBoolean, True if mafft_fa_file is in regular FASTA format.')
    print('-o\t--output_dir=<str>\tLocation of output')
    print('This porgram will receive a multiple alignment FASTA file and return the consensus sequence, determined by simple maximum appearances per position, excluding gaps.\n')
    sys.exit(2)
    return()

def fix_format(mafft_fa_file):
    new_name = ''.join((output_dir, mafft_fa_file.split('.')[0], '_std.fa'))
    better_file = open(new_name, 'w')
    with open(mafft_fa_file, 'r') as f:
        cat_line = ''
        for line in f:
            line = line.strip('\n')
            if '>' in line:
                if cat_line != '':
                    better_file.write(''.join((cat_line, '\n')))
                better_file.write(''.join((line, '\n')))
                cat_line = ''
            else:
                cat_line += line
        better_file.write(''.join((cat_line, '\n')))            
    better_file.close()
    return new_name
    
def create_comp_dict(df_to_comp):
    to_return = {}
    tot_len = len(df_to_comp)
    for i in df_to_comp.columns:
        vals = df_to_comp[i].value_counts()
        G_prop = vals['G'] / tot_len if 'G' in df_to_comp[i].value_counts().keys() else 0
        C_prop = vals['C'] / tot_len if 'C' in df_to_comp[i].value_counts().keys() else 0
        A_prop = vals['A'] / tot_len if 'A' in df_to_comp[i].value_counts().keys() else 0
        T_prop = vals['T'] / tot_len if 'T' in df_to_comp[i].value_counts().keys() else 0
        dash_prop = vals['-'] / tot_len if '-' in df_to_comp[i].value_counts().keys() else 0
        to_return[i] = {'G': G_prop,
                        'C': C_prop,
                        'A': A_prop,
                        'T': T_prop,
                        '-': dash_prop}
    return to_return

def create_consensus(mafft_fa_file, bad_format, output_dir):
    if bad_format:
        path = fix_format(mafft_fa_file)
    else:
        path = mafft_fa_file
    with open(path) as f:
        seq_lst = [list(line.strip('\n')) for line in f if '>' not in line]
    seq_df = pd.DataFrame(seq_lst)
    comp_dict = create_comp_dict(seq_df)
    comp_df = pd.DataFrame.from_dict(comp_dict)
    toDrop = []
    for i in comp_df.columns:
        if comp_df[i][comp_df[i]==max(comp_df[i])].keys()[0]=='-':
            if max(comp_df) > 0.6:
                toDrop.append(i)
    return ''.join(([comp_df[i][comp_df[i]==max(comp_df[i])].keys()[0]for i in comp_df.columns if i not in toDrop]))

if __name__ == '__main__':
    mafft_fa_file, bad_format, output_dir = main(sys.argv[1:])
    if (os.path.isdir(output_dir) == False):
        os.mkdir(output_dir)
        print('Created OUTPUT DIRECTORY: ', output_dir,'\n')
                   
    print('Input files:\n')
    print('mafft_fa_file:\t', mafft_fa_file)
    print('bad_format:\t', bad_format)
    print('Result has been saved in:\t', output_dir)

    final_file = open(''.join(('consensus_from_MAFFT.txt')), 'w')
    final_file.write(create_consensus(mafft_fa_file, bad_format, output_dir))
    final_file.close()
               
    print('Done!\n\n')
        
