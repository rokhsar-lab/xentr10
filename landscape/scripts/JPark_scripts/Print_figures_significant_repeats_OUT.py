# /global/homes/s/sofiamr/SCRIPTS/Python_scripts/JPark_scripts/Print_figures_significant_repeats_OUT.py

import matplotlib
matplotlib.use('Agg')
import pandas as pd
import os
import matplotlib.pyplot as plt

Specie_name         = 'Xenopus tropicalis'
path_wd             ='/global/cscratch1/sd/sofiamr/SAPS/X_tropicalis_V10_Final/IGC_7/RepMask/TRF_Out/'
file_Enrichment     = ''.join((path_wd,'XEN_CONSENSUS_Clust_represent_TRF_Enriched_Cent.tab'))
file_genome_index   = ''.join((path_wd,'XENTRv10-1_Chrs_only.fa.fai'))
file_centromere_pos = ''.join((path_wd,'XENTRv10-1_PCA_centromeres.tab'))
file_blast_out      = ''.join((path_wd,'XEN_CONSENSUS_Clust_represent.blast'))
FDR =0.05
Delta_ = .10
Max_Cent_count = 100
Min_Arm_count = 2000


path_out = ''.join((path_wd,'OUT_Figs/'))
if os.path.isdir(path_out) == False:
    os.makedirs(path_out)
    print "Created ",path_out
Specie_abr  = '_'.join((Specie_name.split(' ')[0][0],Specie_name.split(' ')[1][:4]))
    
Enrichment      = pd.read_csv(file_Enrichment, sep='\t')
Chr_sizes = pd.read_csv(file_genome_index, sep='\t', names=(['Chr','Length','A','B',"C"]))#
Centromere_locations = pd.read_csv(file_centromere_pos, sep='\t', index_col=0)

names = ['qseqid', 'Chr', 'Match percent', 'Length', 'Mismatch', 'Gapopen', 'Consensus start', 'Consensus end', 'sstart', 'send', 'Evalue', 'Bitscore']
Blast_out = pd.read_csv(file_blast_out, sep='\t', names=names)
Blast_out['midpoint'] = (Blast_out.sstart +Blast_out.send)/2


### Figure
Mb = 1000000


To_plot = Enrichment[(Enrichment.fdr_bh<FDR) &(Enrichment.Delta<Delta_) & (Enrichment.IN_Cent<Max_Cent_count) & (Enrichment.NOT_Cent>Min_Arm_count)].sort_values('Delta')
# Enrichment[(Enrichment.fdr_bh<FDR) &(Enrichment.Delta>Delta_min) & (Enrichment.IN_Cent>Min_Cent_count)]

print "X. tropicalis enriched repeats, n=", To_plot.shape[0]
cent_repeat = 'Chr4:46570283-46588687:206,89.4,206'
print "Saving figures for:"
#print "Delta_min: ", Delta_min, "\tMin_Cent_count",Min_Cent_count, "\tFDR",FDR
print "Output figures will be stored  in: ", path_out

fig_counter= 0
for ID_to_plot in To_plot.qseqid.to_list():
    Count_cent = int(Enrichment.loc[Enrichment.qseqid==ID_to_plot,'IN_Cent'].to_list()[0])
    Count_arm = int(Enrichment.loc[Enrichment.qseqid==ID_to_plot,'NOT_Cent'].to_list()[0])

    init_ = 10
    step  = 0.3
    fig = plt.figure(figsize=(18,1), dpi=200)
    first = 1
    longest_chr = float(Chr_sizes.Length.max())
    for Chr_ in Centromere_locations.Chr.unique():
        ax1 = fig.add_axes([0.1, init_, 0.8, step], xlim=(0,longest_chr), ylim=(0,1))
        init_ =  init_ - step
        ax1.tick_params(top=False, bottom=False, left=False, right=False, labelleft=False, labelbottom=False)
        ax1.axvline(Chr_sizes[Chr_sizes.Chr==Chr_].Length.to_list()[0], color='black', alpha=.3, linewidth=1)
        ax1.axvline(0, color='black', alpha=.9, linewidth=1)
        ax1.axhline(y=.5, xmin=0, xmax=Chr_sizes[Chr_sizes.Chr==Chr_].Length.to_list()[0]/float(Chr_sizes.Length.max()), color='black', alpha=0.3, linewidth=1)
        ax1.axis('off')
        if first == 1:
            title = ''.join((Specie_name,'mononer distribution\nBlue: ',ID_to_plot,'\nYellow:',cent_repeat,'\nCent count=',str(Count_cent),';  Arm count=',str(Count_arm),'   (adj_pval<0.05)'))
            ax1.axvline(Chr_sizes[Chr_sizes.Chr==Chr_].Length.to_list()[0]-10, color='black', alpha=.3, linewidth=3)
            plt.title(title)
            first =0
        for list_cents  in zip(Centromere_locations[Centromere_locations.Chr==Chr_].Start,Centromere_locations[Centromere_locations.Chr==Chr_].End):
            ax1.axvline(list_cents[0]-Mb/2, ymin=.4, ymax=.59,color='black', alpha=0.4)
            ax1.axvline(list_cents[1]+Mb/2, ymin=.4, ymax=.59,color='black', alpha=0.4)
            ax1.axhline(y=.4, xmin=(list_cents[0]-Mb/2)/float(longest_chr), xmax=(list_cents[1]+Mb/2)/float(longest_chr), color='black',alpha=0.4, linewidth=1)
            ax1.axhline(y=.6, xmin=(list_cents[0]-Mb/2)/float(longest_chr), xmax=(list_cents[1]+Mb/2)/float(longest_chr), color='black',alpha=0.4, linewidth=1)
        
        for mdpt in   Blast_out[(Blast_out.qseqid==cent_repeat) & (Blast_out.Chr==Chr_)].midpoint.to_list():
            ax1.axvline(mdpt, color='orange', alpha=0.2, linewidth=1)
        for mdpt in   Blast_out[(Blast_out.qseqid==ID_to_plot) & (Blast_out.Chr==Chr_)].midpoint.to_list():
            ax1.axvline(mdpt, color='blue', alpha=0.2,  linewidth=1)

        plt.text(-8000000,.45,Chr_)

    plt.text(longest_chr-20*Mb,1,'20 Mb')
    ax1.axhline(y=.5, xmin=((longest_chr-25*Mb)/longest_chr), xmax=((longest_chr-5*Mb)/longest_chr), color='black', alpha=1)
        
    ax1.tick_params(top=False, bottom=True, left=False, right=False, labelleft=False, labelbottom=True)
    fig_name = ''.join((path_out,Specie_abr, '__MaxDelta',str(int(Delta_)),'__MaxNCent',str(Max_Cent_count), '__Min_Arm_count',str(Min_Arm_count),'__FDR', str(FDR*100)[0], 'pct__FIG_', str(fig_counter),'.png'))
    #fig_name = ''.join((path_out,Specie_abr,'__MinDelta',str(int(Delta_min)),'__MinNCent',str(Min_Cent_count),'__FDR',str(FDR*100)[0],'pct__FIG_',str(fig_counter),'.png'))
    plt.savefig(fig_name, bbox_inches='tight')
    fig_counter= fig_counter+1
    print  fig_counter, "Figures saved", fig_name
    
print "End"
