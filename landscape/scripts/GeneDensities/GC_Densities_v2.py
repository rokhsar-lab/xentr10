#!/usr/bin/python
# Program written by Sofia Medina - April 15, 2019
# This program obtains repeat densities along the chromoson given a Windos size (Mb), a genome index file, and a repeat annotation file.
# Last modified May 3, 2019.

import datetime
import sys, getopt
import pandas as pd
import copy
import numpy as np
import pysam
import os.path

def main(argv):
    WindowSize  = '1'
    RepGFF      = ''
    Repeat_Kind = 'Class'
    ListRepeats = ''
    GC_dens     = 'n'
    Chr_prefix  = 'Chr'
    Output_dir  = ''
    
    try:
        opts, args = getopt.getopt(argv,"hw:r:k:g:G:C:o",["WindowSize=","RepGFF=","RepKind=","Genome=","GC_density=","Chr_prefix=","Output_dir="])
    except getopt.GetoptError:
        print '\tRepeat_Densities_v2.py -w <WindowSize> -r <RepGFF> -k <RepKind> -g <Genome> -G <GC_density> -C <Chr_prefix> -o <Output_dir>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print '\nRepeat_Densities.py -w <WindowSize> -r <RepGFF or Rep.out> -k <RepKind> -g <genome> -G <GC_density> -o <Output_dir>\n'
            print 'About: This program obtains genomic feature densities: GC% or repeats given a repeat annotation file, genome fasta (+index), and a window size (Mb).'
            print 'Code developed by Sofia Medina Ruiz (sofiamr@berkeley.edu), Rokhsar Lab at UC Berkeley.\nLast modification May6,2019\n'
            print '\tHelp commands:'
            print '\t-w, WindowSize=<int>\tMegabase size window (default: 1Mb)\t'
            print '\t-r, RepGFF=<str>\tComplete path to repeat annotation table (.gff or Repeat_masker.out)\t'
            print '\t-k, RepKind=Class|Motif|All|None\tSelect if you want to obtain repeat density by Repeat Class or by Repeat Motif (default: Class)\t'
            print '\t-g, Genome=<str>\tComplete Path to genome fasta file (must be indexed)'
            print '\t-C, Chr_prefix=<str>\tPrefix to chromosome names (default: Chr)'
            print '\t-G, GC_density=Yes|No\tObtain GC% density of the genome (requires genome fasta file; default: no)'
            print '\t-o, Output_dir=<str>\tComplete Path to output directory (must exist)'
            print '\n\n'
            sys.exit()
        elif opt in ("-w", "--WindowSize"):
            WindowSize = int(arg)
        elif opt in ("-r", "--RepGFF"):
            RepGFF = arg
        elif opt in ("-k", "--RepKind"):
            Repeat_Kind = arg
        elif opt in ("-g", "--Genome"):
            Genome = arg
        elif opt in ("-G", "--GC_density"):
            GC_dens = arg
        elif opt in ("-C", "--Chr_prefix"):
            Chr_prefix = arg
        elif opt in ("-o", "--Output_dir"):
            Output_dir = arg

    
    if RepGFF == '':
        print 'Error: Please provide a Repeat annotation <.gff>'
        sys.exit(2)
    return(WindowSize, RepGFF, Repeat_Kind, Genome, GC_dens, Chr_prefix, Output_dir)
    
    
def obtain_parameters(Mb_window_size, Repeat_Kind, genome_file, gff_file, Output_dir):
    Mb                  = 1000000
    window_size         = int(Mb_window_size)*Mb
    slide_window        = int(window_size*.05)
    Specie_name         = gff_file.split('/')[-1].split('.')[0]

    if len(genome_file) == 0:
        genome_file       = gff_file.replace('.gff','.fa')
        genome_index_file = gff_file.replace('.gff','.fa.fai')
    else:
        genome_index_file = ''.join((genome_file,'.fai'))
        
    if os.path.exists(gff_file) == False:
        print "ERROR: Repeat annotation not found. Please check path"
        sys.exit(2)
    if os.path.exists(genome_file) == False:
        print "ERROR: Genome fasta not found. Please check path"
        sys.exit(2)
    if os.path.exists(genome_index_file) == False:
        print "ERROR: Genome fasta index. Please check path"
        sys.exit(2)
    
    path_to_save        = '/'.join((gff_file.split('/')[:-1]))
    if len(Output_dir) >0:
        if os.direxists(Output_dir):
            #file_to_save        = ''.join((path_to_save,'/',Specie_name,'.Repeat_',Repeat_Kind,'_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
            #file_to_save_GC_den = ''.join((path_to_save,'/',Specie_name,'.GC_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
            file_to_save        = ''.join((Output_dir,'/',Specie_name,'.Repeat_',Repeat_Kind,'_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
            file_to_save_GC_den = ''.join((Output_dir,'/',Specie_name,'.GC_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
        else:
            print "ERROR: Output directory doesn't exist"
            sys.exit(2)         
    else:
        path_to_save = '/global/cscratch1/sd/sofiamr/SAPS/X_tropicalis_V10_Final/Density_species'
        print "Will be saving in default directory:", path_to_save
        
        file_to_save_Motif  = ''.join((path_to_save,'/',Specie_name,'.Repeat_Motif_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
        file_to_save_Class  = ''.join((path_to_save,'/',Specie_name,'.Repeat_Class_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
        file_to_save_GC_den = ''.join((path_to_save,'/',Specie_name,'.GC_Density_W',str(Mb_window_size),'Mb_',str(slide_window/1000),'Kb.tab'))
        
    return(window_size, slide_window, genome_index_file, genome_file, file_to_save_Motif, file_to_save_Class, file_to_save_GC_den)
          
    
def obtain_repeat_density(window_size, slide_window, chr_name, chr_size, chr_gene_starts):
    gene_density = dict()
    for bin_ in range(0,chr_size,slide_window):
        gene_density[bin_] = int(len(list(x for x in chr_gene_starts if bin_<=x<=bin_+window_size)))
    ret_df = pd.DataFrame.from_dict(gene_density, orient='index').sort_index().reset_index()
    ret_df['Chr'] = chr_name
    ret_df = ret_df.rename(columns={0:'Number of repeats', 'index':'position'})
    ret_df = ret_df[['Chr','position','Number of repeats']]
    ret_df = ret_df[ret_df.position<chr_size - window_size]
    return(ret_df)

def read_files(genome_index_file,gff_file, Repeat_Kind, Chr_prefix):
    print "Reading Genome index and repeat annotation"
    sys.stdout.flush()
    chr_sizes           = pd.read_csv(genome_index_file, names=['Chr','Length','A','B','C'], sep='\t')
    chr_sizes           = chr_sizes[chr_sizes.Chr.str.startswith(Chr_prefix)]
    
    if gff_file.endswith('.gff') == True:
        rep_table           = pd.read_csv(gff_file, sep='\t', skiprows=[0,1,2], names=['Chr','A','Feature','Start','End','Score','Strand','Frame','ID'])
        rep_table['Motif']  =  rep_table.ID.apply(lambda x: x.split(':')[1].split(';')[0].split('"')[0])
        rep_table['Class']  =  rep_table.ID.apply(lambda x: x.split('"')[3])
        rep_table           = rep_table[rep_table.Chr.str.startswith(Chr_prefix)]
    if gff_file.endswith('.out') == True:
        rep_table = pd.read_csv(gff_file, sep='\s+', skiprows=[0,1,2], names=['Score','div','del','ins','Chr','Start','End','left','Strand','Motif','Class','begin', 'end','r_left','ID'])
        rep_table = rep_table[rep_table.Chr.str.startswith(Chr_prefix)]
        rep_table = rep_table[['Chr','Start','End','Score','Strand','Motif','Class']]
        rep_table = rep_table[rep_table.Chr.str.startswith(Chr_prefix)]
    #     
    #if (Repeat_Kind == 'Motif'):
    #    List_of_repeats     = pd.DataFrame(rep_table[Repeat_Kind].value_counts())
    #    List_of_repeats     = List_of_repeats[List_of_repeats[Repeat_Kind]>=20].index.to_list()
    #if (Repeat_Kind == 'Class'):
    #    List_of_repeats     = pd.DataFrame(rep_table[Repeat_Kind].value_counts())
    #    List_of_repeats     = pd.Series(List_of_repeats[List_of_repeats[Repeat_Kind]>=20].index.tolist())
    #    List_of_repeats     = List_of_repeats[~(List_of_repeats=='Unknown')].tolist()
    return(rep_table, chr_sizes)#, List_of_repeats)

def Obtain_Densities_for_all_Chrs(rep_table,chr_sizes,List_of_repeats, Repeat_Kind, file_to_save):
    Density_Class_Repeats  = pd.DataFrame()
    for class_of_repeat in List_of_repeats:
        print class_of_repeat, datetime.datetime.now()
        sys.stdout.flush()
        rep_select_motif = copy.copy(rep_table[rep_table[Repeat_Kind]==class_of_repeat])
        Rep_motif_all = pd.DataFrame()
        for chr_name in chr_sizes.Chr.tolist():
            # Will obtain density per chromosome
            chr_size = chr_sizes[chr_sizes.Chr==chr_name].Length.tolist()[0]
            chr_rep_starts = np.array(sorted(rep_select_motif[(rep_select_motif.Chr==chr_name)].Start.tolist()))
            Rep_density = copy.copy(obtain_repeat_density(window_size, slide_window, chr_name, chr_size, chr_rep_starts))
            Rep_motif_all = Rep_motif_all.append(Rep_density)
        Rep_motif_all = Rep_motif_all.rename(columns={'Number of repeats':class_of_repeat})
        
        if Density_Class_Repeats.shape[0] ==0:    
            Density_Class_Repeats = Rep_motif_all
        else:
            Rep_motif_all = Rep_motif_all.rename(columns={'Number of repeats':class_of_repeat})
            Density_Class_Repeats =  pd.merge(Density_Class_Repeats, Rep_motif_all[['Chr','position',class_of_repeat]], left_on=['Chr','position'], right_on=['Chr','position'])
    Density_Class_Repeats.to_csv(file_to_save, sep='\t',index=False)
    return()

def get_gc_cont(fasta_file, chrom, start, end):
    seq = fasta_file.fetch(chrom, start, end).upper()
    gc_content = (seq.count("C") + seq.count("G"))/float(1 + len(seq)-seq.count("N"))*100
    return(gc_content)

def obtain_gc_content(fasta_file, window_size, slide_window, chr_name, chr_size):
    gc_content = dict()
    for bin_ in range(0,chr_size,slide_window):
        gc_content[bin_] = get_gc_cont(fasta_file, chr_name, bin_, bin_+window_size)
    ret_df = pd.DataFrame.from_dict(gc_content, orient='index').sort_index().reset_index()
    ret_df['Chr'] = chr_name
    ret_df = ret_df.rename(columns={0:'GC%', 'index':'position'})
    ret_df = ret_df[['Chr','position','GC%']]
    ret_df = ret_df[ret_df.position<chr_size - window_size]
    return(ret_df)

def Obtain_GC_content_all_chrs(fasta_file, chr_sizes, window_size, slide_window, file_to_save_GC_den):
    GC_content_all_chrs = pd.DataFrame(columns=['Chr', 'position', 'GC%'])
    for chr_name in chr_sizes.Chr.unique():
        chr_size = chr_sizes[chr_sizes.Chr==chr_name].Length.tolist()[0]
        print 'Obtaining GC% for ', chr_name
        sys.stdout.flush()
        GC_content = obtain_gc_content(fasta_file, window_size, slide_window, chr_name, chr_size)
        GC_content_all_chrs = GC_content_all_chrs.append(GC_content)
    
    GC_content_all_chrs.to_csv(file_to_save_GC_den, sep='\t', index=False)
    print "Done obtaining GC% for all chromosomes"
    print 'GC% Density out file:\t', file_to_save_GC_den
    sys.stdout.flush()
    return()

if __name__ == "__main__":
    (Mb_window_size, gff_file, Repeat_Kind, genome_file, GC_density, Chr_prefix, Output_dir)   = main(sys.argv[1:])
    (window_size, slide_window, genome_index_file, genome_file, file_to_save_Motif, file_to_save_Class, file_to_save_GC_den) = obtain_parameters(Mb_window_size, Repeat_Kind, genome_file, gff_file, Output_dir)

    print 'Started script:', datetime.datetime.now() 
    print "#### Inputs ####"
    print 'Window Size:\t\t', Mb_window_size, "Mb"
    print 'Repeat Annotation file:\t', gff_file
    print 'Gene Index file:\t', genome_index_file
    print 'Repeat Density Motif out file:\t', file_to_save_Motif
    print 'Repeat Density Class out file:\t', file_to_save_Class
    print 'GC% Density out file:\t\t', file_to_save_GC_den
    
    sys.stdout.flush()
    
    # Will read files and return two pandas dataframe and one lists:
    (rep_table, chr_sizes) = read_files(genome_index_file, gff_file, Repeat_Kind, Chr_prefix)
    
    if ((GC_density == 'Yes' )| (GC_density == 'Y')| (GC_density == 'YES')  | (GC_density == 'yes') | (GC_density == 'y')):
        
        if os.path.exists(file_to_save_GC_den) == False:
            print 'Obtaining GC% density as requested ',Chr_prefix
            sys.stdout.flush()
            fasta_file          = pysam.FastaFile(genome_file)
            Obtain_GC_content_all_chrs(fasta_file, chr_sizes, window_size, slide_window, file_to_save_GC_den)
        else:
            print "Genome density file already created:\t",file_to_save_GC_den
            sys.stdout.flush()
            
    if( (Repeat_Kind == 'None') == False ):
        if os.path.exists(file_to_save_Motif) == False:
            Repeat_Kind = 'Motif'
            List_of_repeats     = pd.DataFrame(rep_table[Repeat_Kind].value_counts())
            List_of_repeats     = List_of_repeats[List_of_repeats[Repeat_Kind]>=20].index.to_list()
            print "Will begin process this many repeat Motifs:\t", len(List_of_repeats)
            print  datetime.datetime.now() 
            print '... be patient...'
            sys.stdout.flush()
            Obtain_Densities_for_all_Chrs(rep_table, chr_sizes, List_of_repeats, Repeat_Kind, file_to_save_Motif)
            print 'Done saving file', file_to_save_Motif
            sys.stdout.flush()
        else:
            print "File Already exists (this means density wasn't obtained):\t", file_to_save_Motif

        if os.path.exists(file_to_save_Class) == False:
            Repeat_Kind = 'Class'
            List_of_repeats     = pd.DataFrame(rep_table[Repeat_Kind].value_counts())
            List_of_repeats     = List_of_repeats[List_of_repeats[Repeat_Kind]>=20].index.to_list()
            print "Will begin process this many repeat Classes:\t", len(List_of_repeats)
            print  datetime.datetime.now() 
            print '... be patient...'
            sys.stdout.flush()
            Obtain_Densities_for_all_Chrs(rep_table, chr_sizes, List_of_repeats, Repeat_Kind, file_to_save_Class)
            print 'Done saving file', file_to_save_Class
            sys.stdout.flush()
        else:
            print "File Already exists (this means density wasn't obtained):\t", file_to_save_Class            
    else:
        print 'Did not obtain density by repeats.'
        sys.stdout.flush()
    print 'Ended script:', datetime.datetime.now()    
    sys.stdout.flush()
